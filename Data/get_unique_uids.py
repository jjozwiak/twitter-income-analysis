import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pylab import *

f = open('tweets_by_city.csv','rb')
df = pd.read_csv(f)

# grouped by location
grouped = df.groupby('match_location')

print grouped.median().head()
med = grouped.median()

# user followers
x = med['median_income']
y = med['user_followers_count']
plt.scatter(x,y)
fit = polyfit(x,y,1)
fit_fn = poly1d(fit)
plot(x,y, 'yo', x, fit_fn(x), '--k')
plt.show()

# user statuses
x = med['median_income']
y = med['user_statuses_count']
plt.scatter(x,y)
fit = polyfit(x,y,1)
fit_fn = poly1d(fit)
plot(x,y, 'yo', x, fit_fn(x), '--k')
plt.show()

# user friends
x = med['median_income']
y = med['user_friends_count']
plt.scatter(x,y)
fit = polyfit(x,y,1)
fit_fn = poly1d(fit)
plot(x,y, 'yo', x, fit_fn(x), '--k')
plt.show()
