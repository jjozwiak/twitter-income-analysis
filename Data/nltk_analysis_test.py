import csv
import nltk
from nltk.corpus import wordnet as wn

TEST_WORDS = ['family', 'friend', 'bud', 'buddy', 'neighbor', 'brother', 'sister', 'mother', 'father', 'in-laws', 'criminal', 'stranger', 'student', 'professor', 'walk', 'ride', 'vote', 'love', 'like', 'celebrity', 'congress', 'everyday', 'chocolate', 'phone', 'birthday']

def main():

    # family and friend synset
    family = wn.synset('family.n.01')
    friend = wn.synset('friend.n.01')

    print 'FAMILY'
    for word in TEST_WORDS:
        print word
        word = wn.synsets(word)[0]
        print family.wup_similarity(word)

    print 
    print 'FRIEND'
    for word in TEST_WORDS:
        print word
        word = wn.synsets(word)[0]
        print friend.wup_similarity(word)
        
        
if __name__ == "__main__":
    main()
