import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pylab import *

f = open('nltk_analysis.csv','rb')
df = pd.read_csv(f)

# grouped by location
#grouped = df.groupby('match_location')
grouped = df

#print grouped.mean().head()
#med = grouped.mean()
med = df

# word count
x = med['median_income']
y = med['word_count']
plt.scatter(x,y)
fit = polyfit(x,y,1)
fit_fn = poly1d(fit)
plot(x,y, 'yo', x, fit_fn(x), '--k')
plt.show()

# pronouns
x = med['median_income']
y = med['pronoun_rate']
plt.scatter(x,y)
fit = polyfit(x,y,1)
fit_fn = poly1d(fit)
plot(x,y, 'yo', x, fit_fn(x), '--k')
plt.show()

# word count
x = med['median_income']
y = med['mention_count']
plt.scatter(x,y)
fit = polyfit(x,y,1)
fit_fn = poly1d(fit)
plot(x,y, 'yo', x, fit_fn(x), '--k')
plt.show()

# pronouns
x = med['median_income']
y = med['family_rate']
plt.scatter(x,y)
fit = polyfit(x,y,1)
fit_fn = poly1d(fit)
plot(x,y, 'yo', x, fit_fn(x), '--k')
plt.show()


# pronouns
x = med['median_income']
y = med['friend_rate']
plt.scatter(x,y)
fit = polyfit(x,y,1)
fit_fn = poly1d(fit)
plot(x,y, 'yo', x, fit_fn(x), '--k')
plt.show()

