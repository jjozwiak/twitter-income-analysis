import csv
import nltk
from nltk.corpus import wordnet as wn

INFILE = 'tweets_by_city_full.csv'

def main():

    # read city median income into 
    output = open('nltk_analysis.csv','wb')
    writer = csv.writer(output)
    
    # family and friend synset
    family = wn.synset('family.n.02')
    friend = wn.synset('friend.n.01')

    # open file
    with open(INFILE, 'rb') as f:
        reader = csv.reader(f)

        # ignore header line
        first = True

        # write new header row
        writer.writerow(['user_location','match_location','text','median_income', 'word_count', 'pronoun_rate', 'family_rate', 'friend_rate'])

        # loop through lines of file
        for row in reader:

            # ignore header
            if first:
                first = False
                continue

            # nltk processing (categorize words)
            text = nltk.word_tokenize(row[2])
            tagged = nltk.pos_tag(text)
            word_count = len(tagged)
            prp = [tag for (word,tag) in tagged].count('PRP')
            
            # determine family theme
            fam_count = 0.0
            friend_count = 0.0
            fam_sum = 0.0
            friend_sum = 0.0
            for word in text:
            
                # get word synset
                try:
                    word = wn.synsets(word)[0]
                except:
                    continue
                    
                try:
                    fam_sum += family.wup_similarity(word)
                    fam_count += 1.0
                except:
                    pass
                    
                try:
                    friend_sum += friend.wup_similarity(word)
                    friend_count += 1.0
                except:
                    pass
            
            if fam_count > 0:
                fam_rate = fam_sum / fam_count
            else:
                fam_rate = 0
            if friend_count > 0:
                friend_rate = friend_sum / friend_count
            else:
                friend_rate = 0

            writer.writerow(row[0:4] + [word_count, (prp / float(word_count)), fam_rate, friend_rate])

    output.close()

if __name__ == "__main__":
    main()
