// clear existing data entry
clear

// import data
insheet using "/Users/jjozwiak/Documents/nltk_analysis.csv"

// create log file
log using nltk_analysis_stata.log, text replace


// show initial regression and scatterplot with best fit, no controls
regress pronoun_rate median_income
graph twoway (lfitci pronoun_rate median_income) (scatter pronoun_rate median_income), ytitle("% of Pronouns per Tweet") xtitle("Median Income") title("Percent of Pronouns in Tweet vs. Median Income") legend(off)
graph export pronoun_vs_income_bestfit.pdf, replace
regress median_income pronoun_rate 
graph twoway (lfitci median_income pronoun_rate) (scatter median_income pronoun_rate), xtitle("% of Pronouns per Tweet") ytitle("Median Income") title("Median Income vs. Percent of Pronouns in Tweet") legend(off)
graph export income_vs_pronoun_bestfit.pdf, replace

// show initial regression and scatterplot with best fit, no controls
regress word_count median_income
graph twoway (lfitci word_count median_income) (scatter word_count median_income), ytitle("Words per Tweet") xtitle("Median Income") title("Words per Tweet vs. Median Income") legend(off)
graph export words_vs_income_bestfit.pdf, replace

// show initial regression and scatterplot with best fit, no controls
regress mention_count median_income
graph twoway (lfitci mention_count median_income) (scatter mention_count median_income), ytitle("Mentions per Tweet") xtitle("Median Income") title("Mentions per Tweet vs. Median Income") legend(off)
graph export mentions_vs_income_bestfit.pdf, replace

// show initial regression and scatterplot with best fit, no controls
regress family_rate median_income
graph twoway (lfitci family_rate median_income) (scatter family_rate median_income), ytitle("Family Synonyms per Tweet") xtitle("Median Income") title("Family Synonyms vs. Median Income") legend(off)
graph export family_vs_income_bestfit.pdf, replace

// friend rate is not significant
regress friend_rate median_income



log close
